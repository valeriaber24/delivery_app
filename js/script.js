const navButtons = document.querySelectorAll('.nav__item');
const productItems = document.querySelectorAll('.product__item');

function handleNavButtonClick(index) {
    productItems.forEach((item, i) => {
        item.style.display = i === index ? '' : 'none';
    });
}

navButtons.forEach((button, index) => {
    button.addEventListener('click', () => handleNavButtonClick(index));
});

const addToCartButtons = document.querySelectorAll('.product__button');

function addToCartHandler(event) {
    const productItem = event.target.closest('.product__item');
    const productImg = productItem.querySelector('.product__img').src;
    const productName = productItem.querySelector('.product__name').textContent;
    const productPrice = productItem.querySelector('.product__price').textContent;

    const product = {
        img: productImg,
        name: productName,
        price: productPrice,
    };

    let cartItems = localStorage.getItem('cartItems');
    if (cartItems) {
        cartItems = JSON.parse(cartItems);
    } else {
        cartItems = [];
    }

    cartItems.push(product);
    localStorage.setItem('cartItems', JSON.stringify(cartItems));
    alert('Product added to cart!');
}

addToCartButtons.forEach(button => {
    button.addEventListener('click', addToCartHandler);
});

const cartContainer = document.querySelector('.product__item--cart');
const cartItems = JSON.parse(localStorage.getItem('cartItems'));
const totalPriceElement = document.querySelector('.footer__name--price');

let totalPrice = 0;

if (cartItems && cartItems.length > 0) {
    cartItems.forEach((item, index) => {
        const productItemCart = document.createElement('div');
        productItemCart.classList.add('product__item--cart');

        const productImgCart = document.createElement('img');

        productImgCart.classList.add('product__img--cart');
        productImgCart.src = item.img;
        productImgCart.alt = '';

        const productNameFlexCart = document.createElement('div');

        productNameFlexCart.classList.add('product__name__flex--cart');

        const productNameCart = document.createElement('div');

        productNameCart.classList.add('product__name--cart');
        productNameCart.textContent = item.name;

        const productPriceCart = document.createElement('div');

        productPriceCart.classList.add('product__price--cart');
        productPriceCart.textContent = item.price;

        const productNumberCart = document.createElement('input');

        productNumberCart.classList.add('product__number--cart');
        productNumberCart.type = 'number';
        productNumberCart.min = '0';
        productNumberCart.max = '100';
        productNumberCart.step = '1';
        productNumberCart.value = '0';
        productNumberCart.addEventListener('input', () => {

            const quantity = parseInt(productNumberCart.value);
            const price = parseFloat(productPriceCart.textContent);
            const subTotal = quantity * price;

            totalPrice -= parseFloat(productPriceCart.dataset.subtotal) || 0;
            totalPrice += subTotal;
            productPriceCart.dataset.subtotal = subTotal;

            totalPriceElement.textContent = totalPrice.toFixed(2);
        });

        const deleteButton = document.createElement('button');
        deleteButton.classList.add('delete-button');
        deleteButton.textContent = 'Delete';

        deleteButton.addEventListener('click', () => {
            const subTotal = parseFloat(productPriceCart.dataset.subtotal) || 0;
            totalPrice -= subTotal;

            cartItems.splice(index, 1);
            localStorage.setItem('cartItems', JSON.stringify(cartItems));
            productItemCart.remove();

            if (cartItems.length === 0) {
                const emptyCartElement = document.createElement('div');
                emptyCartElement.classList.add('product__item--cart');
                emptyCartElement.textContent = 'Empty cart';
                cartContainer.appendChild(emptyCartElement);
            }
            totalPriceElement.textContent = totalPrice.toFixed(2);
        });

        productNameFlexCart.appendChild(productNameCart);
        productNameFlexCart.appendChild(productPriceCart);
        productItemCart.appendChild(productImgCart);
        productItemCart.appendChild(productNameFlexCart);
        productItemCart.appendChild(productNumberCart);
        productItemCart.appendChild(deleteButton);
        cartContainer.appendChild(productItemCart);
    });
} else {
    const emptyCartElement = document.createElement('div');
    emptyCartElement.classList.add('product__item--cart');
    emptyCartElement.textContent = 'Empty cart';
    cartContainer.appendChild(emptyCartElement);
}

//////////////////////

const submitButton = document.querySelector('.footer__button');

submitButton.addEventListener('click', () => {
    const cartItems = JSON.parse(localStorage.getItem('cartItems'));
    fetch('link', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(cartItems)
    })
        .then(response => {
            if (response.ok) {
                alert('Order saved in the database!');
                localStorage.removeItem('cartItems');
            } else {
                alert('Failed to save the order in the database.');
            }
        })
        .catch(error => {
            console.log('An error occurred:', error);
        });
});